package ru.tsc.felofyanov.tm.api.repository;

import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    Task create(String name);

    List<Task> findAll();

    void remove(Task task);

    Task create(String name, String description);

    Task add(Task task);

    void clear();
}
