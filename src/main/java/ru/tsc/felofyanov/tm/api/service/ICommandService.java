package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Command;

public interface ICommandService {
    Command[] getCommands();
}
