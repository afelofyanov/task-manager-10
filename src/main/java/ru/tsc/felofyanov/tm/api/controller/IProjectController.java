package ru.tsc.felofyanov.tm.api.controller;

public interface IProjectController {
    void showProjectList();

    void clearProject();

    void createProject();
}
