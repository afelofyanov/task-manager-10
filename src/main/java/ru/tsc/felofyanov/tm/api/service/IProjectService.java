package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    Project add(Project project);

    List<Project> findAll();

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();
}
