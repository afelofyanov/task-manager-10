package ru.tsc.felofyanov.tm.api.service;

import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    Task add(Task task);

    List<Task> findAll();

    void remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();
}
