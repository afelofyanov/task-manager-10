package ru.tsc.felofyanov.tm.controller;

import ru.tsc.felofyanov.tm.api.controller.IProjectController;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProject() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[DONE]");
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
